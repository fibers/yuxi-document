# Killua - a log analysis system 

#### Author: [fibers](https://github.com/fibers)

Killua 是我们即将要做的日志分析系统（我们是一个做面向二次元人群的公司，我这儿选择了我最喜欢的动漫里的人物：全职猎人中的奇犽作为了名称)。  
通过分析日志我们可以得到当前app的一个使用情况和统计信息，有助于我们的广告投放，运营策略，包括产品的决策。


## API服务端的数据提供

#### 数据存储方式

现在Log的记录一般2种方式：  

* 文件  
* database
	
由于现在api服务器是基于windows, 如果是以文件形式记录会需要在*windows server*和*killua server (linux)*之间增加一些没必要的文件拷贝。  

database的话包括RDBMS的mysql, 分布式的HBase, Nosql的mongodb, 更轻量级的缓存  
考虑到当前的需求和时间，选择最方便的缓存方式。


#### 具体实现

API服务器接收到的所有http请求时，往redis里的一个list **`lpush`** 一条json数据。[^1]  

[^1]:Killua会有一个守护进程实时消费每条数据。所以不用担心会导致redis里的list暴满。

**Redis**  

|host|port|list key|
|---|---|---|
|123.57.177.2|6379|api:log|

**Json**

所有json数据请勿必加上以下4个字段
 
|field|description|example|
|---|---|---|
|action|请求动作，其实就是request的下划线形式，全小写|请求`SubmitPersonInfo`,该处填`submit_person_info`|
|uid|我们自己生成的user id，如果像Register这种就用生成后的uid,其他的都可以用参数传过来的uid|5000000|
|timestamp|当前的时间, unix时间戳|1443075810|
|result|当前请求返回的result字段的值|1|

### 需要记录的请求

下面列出的所有请求都是要记log的，如有额外添加字段我会在该请求的名称下加以说明。

	Node:
		下面每个请求我是对着"服务器接口列表.xlsx"做的。如果有些文档里有的接口但是没有实现的，请直接忽略。
	
**`parameter`** : 代表该字段的值引用客户端传过来参数的*parameter*的值
  

####	Register

|field|description|example|
|---|---|---|
|register_type|第三方注册的类型|bd/qq/sn/mb|


#### SubmitPersonInfo

#### SubmitDeiceInfo

|field|description|
|---|---|
|device_id|**`device`**|
|lon|**`lon`**|
|lat|**`lat`**|
|producer|**`producer`**|

#### GetFriendList

#### GetPersonInfo

|field|description|
|---|---|
|puid|**`personid`**|

#### SearchPerson

|field|description|
|---|---|---|
|keyword|**`cond`**|

#### DeleteFriend

|field|description|
|---|---|
|puid|**`oid`**|

#### ReportPerson

|field|description|
|---|---|
|puid|**`oid`**|
|reason_id|**`reason`**|

#### AddFavoriteEmotion

#### DelFavoriteEmotion

#### GetFavoriteEmotionList

#### ModifyUserName

|field|description|
|---|---|
|puid|**`personid`**|

#### GetUserName

#### GetFriendListWithGroup

#### CreateFriendsGroup

|field|description|
|---|---|
|friend_group_id|创建的firend group id|

#### UpdateFriendGroupInfo
|field|description|
|---|---|
|friend_group_id|**`friendgroupid`**|

#### DelFriendsGroup
|field|description|
|---|---|
|friend_group_id|**`friendgroupid`**|

#### ChangeFriendsGroupMember

#### NewFamily

#### ModifyFamilyInfo

|field|description|
|---|---|
|family_id|**`familyid`**|

#### DelFamily

|field|description|
|---|---|
|family_id|**`familyid`**|

#### SearchFamily

|field|description|
|---|---|
|keyword|**`cond`**|

#### GetFamilyList

|field|description|
|---|---|
|index|**`index`**|
|count|**`count`**|

#### FamilySign

|field|description|
|---|---|
|family_id|**`familyid`**|

#### ExitFamily

|field|description|
|---|---|
|family_id|**`familyid`**|

#### GetMyFamilyList

#### GetFamilyInfo

|field|description|
|---|---|
|family_id|**`familyid`**|

#### ReportReason

#### ReportFamily

|field|description|
|---|---|
|family_id|**`familyid`**|
|reason_id|**`reason`**|

#### GetGroupDetail

|field|description|
|---|---|
|group_id|**`groupid`**|


#### ModifyFamilyPersonInfo

|field|description|
|---|---|
|family_id|**`groupid`**|

#### GetFamilyListByType

|field|description|
|---|---|
|family_type|**`type`**|
|index|**`index`**|
|count|**`count`**|

#### GetStorys

#### GetPlayingStory

|field|description|
|---|---|
|index|**`index`**|
|count|**`count`**|

#### GetMyFavoriteStorys

#### GetMyStory

#### SearchStory

|field|description|
|---|---|
|keyword|**`cond`**|
|category|**`category`**|

#### CreateStory

|field|description|
|---|---|
|story_id|创建的story_id|
|group_id|**`groupid`**|
|category|**`category`**|

#### AddGroupToStory

|field|description|
|---|---|---|
|story_id|**`storyid`**|
|group_id|**`groupid`**|
|is_play|**`isplay`**|

#### ModifyStory

|field|description|
|---|---|
|story_id|**`storyid`**|
|modify_type|**`type`**|

#### PraiseStory

|field|description|
|---|---|
|story_id|**`storyid`**|
|behavior|**`behavior`**|

#### SettingRoleInStory

|field|description|
|---|---|
|story_id|**`storyid`**|

#### ReportStory

|field|description|
|---|---|
|story_id|**`id`**|
|reason_id|**`reason`**|

#### GetStoryInfo

|field|description|
|---|---|---|
|story_id|**`storyid`**|

#### GetSelfStory

#### GetBanners

#### CreateStoryScene

|field|description|
|---|---|---|
|story_id|**`storyid`**|

#### CreateStoryRole

|field|description|
|---|---|---|
|story_id|**`storyid`**|

#### ModifyStoryScene

|field|description|
|---|---|---|
|story_id|**`storyid`**|

#### ModifyStoryRole

|field|description|
|---|---|---|
|story_id|**`storyid`**|

#### GetStoryGroups

|field|description|
|---|---|---|
|story_id|**`storyid`**|

#### GetStoryScenes

|field|description|
|---|---|---|
|story_id|**`storyid`**|

#### GetStoryRoles

|field|description|
|---|---|---|
|story_id|**`storyid`**|

#### CreateSelfStory

|field|description|
|---|---|
|story_id|创建的self story id|
|word_count|count(**`content`**):自戏的字数|

#### GetSelfStoryDetail

|field|description|
|---|---|
|story_id|**`storyid`**|

#### GetStoryCategory

#### GetStoryGroupInfo

|field|description|
|---|---|
|story_id|**`storyid`**|
|group_id|**`groupid`**|

#### SetGroupPerssion

|field|description|
|---|---|
|story_id|**`storyid`**|
|group_id|**`groupid`**|
|permission|**`permission`**|

#### GetEnteredStoryGroups

|field|description|
|---|---|
|story_id|**`storyid`**|

#### MofiyStoryGroupInfo

|field|description|
|---|---|
|story_id|**`storyid`**|
|group_id|**`groupid`**|

#### GetStoryPersonInfo

|field|description|
|---|---|
|story_id|**`storyid`**|
|puid|**`pid`**|

#### DeleteStory

|field|description|
|---|---|
|story_id|**`storyid`**|

#### DeleteMyStory

#### GetStoryContent

|field|description|
|---|---|
|story_id|**`storyid`**|

#### ModifyStoryContent

#### GetStoryByFavor

|field|description|
|---|---|
|index|**`index`**|
|count|**`count`**|

#### GetGroupHistoryMsg

|field|description|
|---|---|
|group_id|**`groupid`**|

#### ModifySelfStoryContent

|field|description|
|---|---|
|story_id|**`storyid`**|
|word_count|count(**`content`**):自戏的字数|

#### GetStoryRoleTemplate

|field|description|
|---|---|
|story_id|**`storyid`**|

#### ModifyStoryRoleTemplate

|field|description|
|---|---|
|story_id|**`storyid`**|

#### GetUserRoleInfoInStory

|field|description|
|---|---|
|story_id|**`storyid`**|

#### SetUserRoleInfoInStory

|field|description|
|---|---|
|story_id|**`storyid`**|

#### GetStorysByType

|field|description|
|---|---|
|story_type|**`type`**|
|index|**`index`**|
|count|**`count`**|

#### GetStoryShowType

#### GetStoryShowTypeNew

#### CollectStory

|field|description|
|---|---|
|story_id|**`storyid`**|
|behavior|**`behavior`**|
|is_not_self_story|**`type`**|

#### GetSelectedStorys

#### UpdateStoryGroupBoard

|field|description|
|---|---|
|group_id|**`groupid`**|

#### GetStoryGroupBoard

|field|description|
|---|---|
|group_id|**`groupid`**|

#### NewMessageSetting

|field|description|
|---|---|
|setting|`setting`|

#### HelpMessage

#### SendCustomProblem

|field|description|
|---|---|
|qq|**`qqno`**|

#### GetUnreadNotify

#### UpdateNotifyState

|field|description|
|---|---|
|from_uid|**`fromid`**|
|to_uid|**`toid`**|
|group_id|**`groupid`**|
|notify_type|**`type`**|

#### GetContact

#### GetPropertyInfo

#### GetConfiguration

|field|description|
|---|---|
|from|**`from`**|

#### VerifyInviteCode

#### PublishTopic

|field|description|
|---|---|
|group_id|**`groupid`**|
|word_count|count(**`content`**):话题的字数|

#### GetTopics

#### GetMyTopics

#### RecommendStory

|field|description|
|---|---|---|
|story_id|**`storyid`**|

#### GetRecommendStorys

#### GetTopicDetail

|field|description|
|---|---|---|
|topic_id|**`topicid`**|

#### GetCommentGroupDetail

|field|description|
|---|---|---|
|group_id|**`groupid`**|

#### PraiseTopics

|field|description|
|---|---|---|
|topic_id|**`topicid`**|
|behavior|**`behavior`**|

#### GetMyCommentGroupsInfo

#### GetLatestPackageInfo

|field|description|
|---|---|---|
|device_type|**`devicetype`**|
|producer|**`producer`**|

#### CommitDebugInfo

|field|description|
|---|---|---|
|os_version|**`version`**|
|device|**`device`**|
|debug_info|**`debuginfo`**|

#### GetAllPersonIds


