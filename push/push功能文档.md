# Push system

#### Author: [fibers](https://github.com/fibers)
#### Code: [yuxi-push](https://github.com/HiggsYuxi/yuxi-push)

现有的推送逻辑和当前业务耦合太紧，push功能分散在各个http或tcp的请求里，对以后的业务扩展和运营的扩展十分不利。在此单独抽象出来一个推送的业务层，供api,crm或者是后续的数据系统使用。   
**我们集成的是个推的推送业务**



## Deployment
考虑到当间的业务量，为了节省机器，开发环境和正式环境都在一台服务器上，以端口区分。

|environment|endpoint|
|---|---|
|development|http://120.76.65.85:8080|
|production|http://120.76.65.85|




## Functionality

#### /register_push
```
绑定我们的用户id和个推id
```

|parameter|description|required|
|---|---|---|---|
|user_id|我们自己app的user id|Yes|
|client_id|个推返回的client id|Yes|
|device_id|设备唯一标识符, android为设备id, ios为device token|Yes|
|device_type|设备类型 "android" or "ios"|Yes|

> 该接口可以重复调用，新的值覆盖旧的值。


#### /unregister_push

```
解绑我们的用户id和个推id，因为ios用户走的是苹果自己的apns, 所以在用户logout后，
再继续收到push消息会影响用户体验。需要ios端在用户logout后，调用该接口。Android端最好也是统一处理，这样能保证数据的一致性。
```

|parameter|description|required|
|---|---|---|
|user_id|我们自己app的user id|Yes|

> 该接口可以重复调用


#### /push_to_user

```
推送一条消息到我们的某个用户。
```

|parameter|description|required|
|---|---|---|
|user_id|我们自己app的user id|Yes|
|title|当消息弹出时，展示的标题，只有Android用户才会出现标题|No|
|text|当消息弹出时，展示的内容|No|
|payload|消息附带的其他信息，用来做一些相应的处理，是一个json的字符串|No|

#### /push_to_users

```
推送一条消息到我们的特定用户群。
```

|parameter|description|required|
|---|---|---|
|user_ids|我们自己app的user id，是一个表示数组的字符串，一次最多60个用户id|Yes|
|title|当消息弹出时，展示的标题，只有Android用户才会出现标题|No|
|text|当消息弹出时，展示的内容|No|
|payload|消息附带的其他信息，用来做一些相应的处理，是一个json的字符串|No|


## Payload Definition
payload消息不会展示给用户看，这里面携带的信息，app端可以用来做跳转，ui变化，以及一些其他的对应操作。***下面列出的部分定义可能当前已经实现，或实现方式和定义的不一样，以后再一起商量调整，这儿列出只是为了举例说明一些使用场景***

```
{
	"t": "s",
	"a": "xxxxxx"
}		
```
palyload大概是以上格式。

* t(*type*) : 当前这条消息是类型，值当前只有s，即是system message, 包括,用户之前的互动，谁回了我的帖，谁拜我为师了，运营给推了一个剧，推荐了一个人等等都走这种消息类型。这种消息类型一般都是点击完会跳转到相应的页面。

* a(*action*) : 当前这条消息确切的动作，有些消息的action是不需要参数的，比如当值是ma(Master and Apprentice 师徒关系)时，点击消息将跳转到师徒各种消息的页面。有些消息的action是需要参数的，比如当值是u:1000190, 点击消息将跳转到用户id为1000190的用户页。 
> 考虑到这个json不是一个特别格式化的数据，可能各个动作的差异化数据不太合适用一个严格的json结构来表达，所以才会想用这种只有一级的扁平化方式来传action数据, 比如u:1000190

#### 跳转到用户个人页
```
{
    "t": "s",
    "a": "u:{user_id}"
}
```

#### 跳转到剧的详情页
```
{
    "t": "s",
    "a": "s:{story_id}"
}
```

#### 跳转到自戏的详情页
```
{
    "t": "s",
    "a": "ss:{sefstory_id}"
}
```

#### 跳转到某个广场帖的详情页
```
{
    "t": "s",
    "a": "p:{post_id}"
}
```

**待添加......**





